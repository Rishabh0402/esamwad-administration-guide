# Esamwad Administration Guide
> The file contains the administraion guide for Esamwad Admin Dashboard.

## Website Home
The following image shows the home page of website. To Login as Admin you've to click on **Admin login** link on the page.

![Website Home](images/eSamwad46.png)

## Login Screen
The following image shows the Admin login interface.
You have to enter your login credentials in the respective fields to access the Admin Dashboard.

![Login Screen](images/eSamwad45.png)

## Admin Home page
The following image shows the Admin Dashboard.

**Note** - You might see a little different interface based on your **privileges** and your *theme* and *widget* selection and positioning.

![Admin Home page](images/eSamwad44.png)

## Admin Sidebar
The below image shows the Dashboard sidebar. The sidebar is dynamic in nature, means it hides itself when not in use or toggled, so as to make more space for the content on the web page. It has a toggle button (The 3 dashes on the top left corner) which helps it in displaying and hiding itself. But you can fix its position as explained in the [next section](#admin-sidebar-fixed).

![Admin Sidebar](images/eSamwad43.png)

## Admin Sidebar Fixed
To fix the sidebar and make it always visible you can click on the **Push pin** button right next to the dashboard title *ESAMWAD ADMINISTRATION DASHBOARD*. Once fixed the push pin button changes its alignment from **vertical pin** to **horizontal pin**.

**Note** - You can see the difference in the image of [this section](#admin-sidebar-fixed) and the image of [previous section](#admin-sidebar).

![Admin Sidebar Fixed](images/eSamwad42.png)

## Admin Sidebar Content
The Admin Sidebar has all the content you need at one place, which makes it easily accessible.

Below is the explanation of the available functions/buttons:
1. **HOME** - It refers to the Home page of Admin Dashboard which is shown in [Admin Home page Section](#admin-home-page) and has the URL - `https://esamwad.samagra.io/admin/` .

2. **VIEW SITE** - It refers to the home page of the website which is shown in [Website Home Section](#website-home) and has the URL - `https://esamwad.samagra.io/` .

**SERVER**

3. **Locations**  - This function/button has the locations in which all the schools are present. The details about this function are available in the [Locations Section](#locations). It has the URL - `https://esamwad.samagra.io/admin/server/location/` .

4. **Deadlines** - This function/button has the deadlines available for all assessments. The details about this function are available in the [Deadlines Section](#deadlines). It has the URL - `https://esamwad.samagra.io/admin/server/deadline/` .

5. **Assessments** - This function/button has the information for all available assessments, and can be used to create new assessments. The details about this function are available in the [Assessments Section](#assessments). It has the URL - `https://esamwad.samagra.io/admin/server/assessment/` .

6. **Subjects** - This function/button has the information for all available subjects, and can be used to create new subjects. The details about this function are available in the [Subjects Section](#subjects). It has the URL - `https://esamwad.samagra.io/admin/server/subject/` .

7. **Streams** - This function/button has the information for all available streams, and can be used to create new streams for user convenience. The details about this function are available in the [Streams Section](#streams). It has the URL - `https://esamwad.samagra.io/admin/server/stream/` .

8. **Classes** - This function/button has the information for all available classes (also referred as grades at places), and can be used to create new classes and sections. The details about this function are available in the [Classes Section](#classes). It has the URL - `https://esamwad.samagra.io/admin/server/grade/` .

9. **LOs** - This function/button has the information for all available LOs, and can be used to create new LOs. The details about this function are available in the [LOs Section](#los). It has the URL - `https://esamwad.samagra.io/admin/server/lo/` .

10. **Questions** - This function/button has the information for all available questions, and can be used to create new questions. The details about this function are available in the [Questions Section](#questions). It has the URL - `https://esamwad.samagra.io/admin/server/question/` .

11. **Schools** - This function/button has the information for all available schools, and can be used to create new schools. The details about this function are available in the [Schools Section](#schools). It has the URL - `https://esamwad.samagra.io/admin/server/school/` .

12. **Students** - This function/button has the information for all available students, and can be used to create new students. The details about this function are available in the [Students Section](#students). It has the URL - `https://esamwad.samagra.io/admin/server/student/` .

13. **BOOKMARKS** - This button can be used on any page to bookmark the location of the page.

![Admin Sidebar Content](images/eSamwad41.png)

## Admin Dashboard Color scheme
This option allows the user to select a color scheme based on his preferences. The option would appear when the use hovers the cursor over his username option on the top right corner.

![Admin Dashboard Color scheme](images/eSamwad0.png)

## Locations
This option allows the user to view all the available options. The user can add/update/delete any location based on his privileges.

![Locations](images/eSamwad40.png)

## Adding Location
To add a new location user has to fill the form available in the following sections.

### Select/Search District from Dropdown
The user can choose the available districts or can search for the districts in the available box section.

![Add District from Dropdown](images/eSamwad39.png)

### Location form Filled
After the user has filled the form Completely. He can save the form using the available buttons.

![Location form Filled](images/eSamwad38.png)

## Location created response
After the user saves the form he can see the status of his action.

![Location created response](images/eSamwad32.png)

## Deadlines
This option allows the user to view all the available options. The user can add/update/delete any deadline based on his privileges.

![Deadlines](images/eSamwad37.png)

## Adding Deadline
To add a new deadline user has to fill the form available in the following sections.

### Select/Search academic year from dropdown
The user can choose the available academic years or can search for them in the available box section.

![Add academic year from dropdown](images/eSamwad36.png)

### Select date from dropdown
The user can select a date from the calendar.

![Add date from dropdown](images/eSamwad35.png)

### Select session from dropdown
The user can choose the available sessions or can search for them in the available box section.

![Add session from dropdown](images/eSamwad34.png)

### Deadline created response
After the user saves the form he can see the status of his action.

![Deadline created response](images/eSamwad33.png)

## Assessments
This option allows the user to view all the available options. The user can add/update/delete any assessment based on his privileges.

![Assessments](images/eSamwad31.png)

## Adding Assessment
To add a new assessment user has to fill the form available in the following sections.

### Select/Search assessment type from dropdown
The user can choose the available assessments or can search for them in the available box section.

![Add assessment type from dropdown](images/eSamwad30.png)

### Select/Search deadline from dropdown
The user can choose the available deadlines or can search for them in the available box section.

![Select deadline from dropdown](images/eSamwad29.png)

### Select grades and sections from choices
The user can choose the available grades/class-sections or can search for them in the available box section.

![Select grades and sections from choices](images/eSamwad28.png)

### Assessment created response
After the user saves the form he can see the status of his action.

![Assessment created response](images/eSamwad27.png)

## Subjects
This option allows the user to view all the available options. The user can add/update/delete any subject based on his privileges.

![Subjects](images/eSamwad26.png)

### Adding Subject
To add a new subject user has to fill the form shown in the following image.

![Adding Subjects](images/eSamwad25.png)

### Subject created response
After the user saves the form he can see the status of his action.

![Subject created response](images/eSamwad24.png)

## Streams
This option allows the user to view all the available options. The user can add/update/delete any stream based on his privileges.

![Streams](images/eSamwad23.png)

## Adding Streams
To add a new stream user has to fill the form available in the following sections.

### Select/Search subjects from dropdown
The user can choose the available subjects or can search for them in the available box section.

![Adding Streams](images/eSamwad22.png)

### Stream created response
After the user saves the form he can see the status of his action.

![Stream created response](images/eSamwad21.png)

## Classes
This option allows the user to view all the available options. The user can add/update/delete any classes based on his privileges.

![Classes](images/eSamwad20.png)

### Class created response
After the user saves the form he can see the status of his action.

![Class created response](images/eSamwad19.png)

## LOs
This option allows the user to view all the available options. The user can add/update/delete any LO based on his privileges.

![LOs](images/eSamwad18.png)

## Adding LO
To add a LO location user has to fill the form available in the following sections.

### Select/Search assessment from dropdown
The user can choose the available assessments or can search for them in the available box section.

![Select assessment from dropdown](images/eSamwad17.png)

### Select/Search subjects from choices
The user can choose the available subjects or can search for them in the available box section.

![Select subjects from choices](images/eSamwad16.png)

### LO created response
After the user saves the form he can see the status of his action.

![LO created response](images/eSamwad15.png)

## Questions
This option allows the user to view all the available options. The user can add/update/delete any question based on his privileges.

![Questions](images/eSamwad11.png)

## Add Question
To add a new question user has to fill the form available in the following sections.

### Select/Search LO from dropdown
The user can choose the available LOs or can search for them in the available box section.

![Select LO from dropdown](images/eSamwad10.png)

### Question created response
After the user saves the form he can see the status of his action.

![Question created response](images/eSamwad9.png)

## Schools
This option allows the user to view all the available options. The user can add/update/delete any school based on his privileges.

![Schools](images/eSamwad8.png)

## Add School
To add a new school user has to fill the form available in the following sections.

### Select/Search location from dropdown
The user can choose the available locations or can search for them in the available box section.

![Select location from dropdown](images/eSamwad7.png)

### Adding/Searching grades and session from choices and dropdown respectively
The user can choose the available grades and sessions or can search for them in the available box section.

![Adding grades and session from choices and dropdown respectively](images/eSamwad6.png)

### School created response
After the user saves the form he can see the status of his action.

![School created response](images/eSamwad5.png)

## Students
This option allows the user to view all the available options. The user can add/update/delete any student based on his privileges.

![Students](images/eSamwad4.png)

## Add Student
To add a new student user has to fill the form available in the following sections.

### Select/Search school
The user can choose the available schools or can search for them in the available box section.

![Select/Search school](images/eSamwad3.png)

### Completely filled student adding form
After the user has filled the form Completely. He can save the form using the available buttons.

![Completely filled student adding form](images/eSamwad2.png)

### Student created response
After the user saves the form he can see the status of his action.

![Student created response](images/eSamwad1.png)
